// Experess and modules
const express = require("express");
const bodyParser = require("body-parser");
const path = require("path");

const app = express();

app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');
app.use(express.static(__dirname + '/public'));


const port = "8080";

const jsonParser = bodyParser.json();

app.use(express.json());
app.use(express.urlencoded());
app.use(express.static('public'));




//Verify Auth

const {VerifyUserToken, VerifyAdminToken} = require('./middleware/middleware')

//Import Controller

const {registerUser, loginUser, updateUser, deleteUser, adminView} = require('./controller/controller')


// Chapter 3

app.get('/c-3', (req, res) => {
  res.render('c-3')
});

// Chapter 4

app.get('/c-4', (req, res) => {
  res.render('c-4')
});

// UsersList

let users = require('./data/users')

//Create User Profile

app.post('/register', jsonParser, registerUser);

//Read User Profile

app.post('/login', jsonParser, loginUser);

//Update Profile

app.patch('/edit', jsonParser, VerifyUserToken, updateUser);


//Delete Profile
app.delete('/delete', jsonParser, VerifyUserToken, deleteUser);


// View All Profile 
app.get('/view-all', jsonParser, VerifyAdminToken, adminView);



//Server Port Listen
app.listen(port, () => {
    console.log(`App listening at http://localhost:${port}`);
  });