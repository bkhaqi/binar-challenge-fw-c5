let users = require('../data/users');
let adminToken = "admin";

exports.VerifyAdminToken = function(req, res, next) {
    let auth = req.headers['authorization'];
    if (adminToken === auth) {
      next();
    }else {
      res.status(401).json({
        message: "Unauthorized"
      });
    };
  };
  
exports.VerifyUserToken = function(req, res, next) {
    let auth = req.headers['authorization'];
    let body = req.body;
    let email = body.email;
    let user = users.find(u => u.email === email);
    if (user && user.token === auth ) {
      next();
    } else {
      res.status(401).json({
        "message": "Unauthorized"
      });
    };
  };