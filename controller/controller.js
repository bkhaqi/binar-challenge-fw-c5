let users = require('../data/users')

exports.registerUser = function(req, res) {
  let body = req.body;
  let id = users.length + 1;
  let username = body.username;
  let email = body.email;
  let password = body.password;
  let token = (Math.random() + 1).toString(36).substring(7);
  // User Checker
  let checkUserEmail = users.find(u => u.email === email)
     if(checkUserEmail) {
        res.send({
            "message": "Error ! Email already registered"
         });
         return
     };
  let checkUserUsername = users.find(u => u.username === username)
     if(checkUserUsername) {
        res.send({
            "message": "Error ! Username already registered"
         });
         return
     };

  users.push({ id,
              username,
              email, 
              password, 
              token 
            });

  console.log(`User ${username} Berhasil Register`);
  console.log(`ID user = ${id}`)
  console.log(body)
          
  res.send({
    message: "Account Registered, You Must Login to see your token!",
    accountID : id
  });
};

exports.loginUser = function(req, res) {
  let body = req.body;
  let email = body.email;
  let password = body.password;

  let u = users.find(u => u.email === email);

  if (u && u.password === password) {
    res.status(200).json({
      message: "Login Success",
      user_info: u,
      Warning: "Do not share your token!"
    })
  }else {
    res.status(404).json({
      message: "Wrong Password or Email!"
    })
  }
};

exports.updateUser = function (req, res) {
  let body = req.body;
  let email = body.email;
  let username = body.username;
  let password = body.password;

  let u = users.find(u => u.email === email);
  if (u && u.password === password){
      let newPassword = body.newPassword
      u.username = username;
      u.password = newPassword;

      users.map(o => o.email === u.email ? u : o);

      res.json({
        message: "User updated",
        user_info: u
      });
  } else {
    res.status(404).json({
      message: "Wrong Password or Email!"
    });
  };
};

exports.deleteUser = function(req, res) {
  let body = req.body
  let email = body.email
  let password = body.password

  let user = users.find(u => u.email == email);
  if (user && user.password === password) {
    users = users.filter(u => u.email != email);

    res.json({
      message: "Account Deleted"
    });
  } else {
    res.status(404).json({
      message: "Account not found"
    });
  }
};

exports.adminView = function(req, res) {
    res.json(users);
};